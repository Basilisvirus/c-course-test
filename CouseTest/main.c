/*This source code copyrighted by Lazy Foo' Productions (2004-2022)
and may not be redistributed without written permission.*/

//Using SDL and standard IO
#include <SDL.h>
#include <stdio.h>

#define SCREEN_WIDTH 800
#define SCREEN_HEIGHT 600


int main( int argc, char* args[] )
{

	//Initialize SDL
    if (SDL_Init( SDL_INIT_VIDEO ) < 0)
    {
        printf("sdl init error %s\n", SDL_GetError());
    }
    else
    {
        printf("SDL init sucessful\n");
    }

    SDL_Window * window = SDL_CreateWindow(
                                           "Hunger Games"
                                           ,0,0,
                                           SCREEN_WIDTH,
                                           SCREEN_HEIGHT,
                                           SDL_WINDOW_RESIZABLE
                                           );

    if(window == NULL)
    {
        printf("Window creation error: %s\n", SDL_GetError());
    }
    else
    {
        printf("Window created\n");
    }

    SDL_Renderer * renderer = SDL_CreateRenderer(window, -1, SDL_RENDERER_ACCELERATED);

    if(renderer == NULL)
    {
        printf("Renderer creation error: %s\n", SDL_GetError());
    }
    else
    {
        printf("Renderer created\n");
    }

    int quit = 0;

    while(!quit)
    {
        SDL_Event event;
        while(SDL_PollEvent(&event))
        {
            switch(event.type)
            {
            case SDL_Quit:
                {
                    quit=1;
                }break;
            }
        }
        if (SDL_RenderClear(renderer) < 0)
        {
            //TODO
        }
    }

	//Quit SDL subsystems
	SDL_Quit();

	return 0;
}
